#ifndef _DEFINES_H
#define _DEFINES_H

#include "p33Fxxxx.h"			// generic file header
#include <libq.h>

// Board type
//#define V1_3            // uncomment for version 1.3
//#define V1_5            // uncomment for version 1.5 (V1_3 must also be defined--TODO:fix)
//#define USE_UDP           // uncomment if you are using the wifi wifly modules instead of xbees
//#define GYRO_TEST       // uncomment for performing led gyro test
//#define ACCEL_TEST      // uncomment for performing led accelerometer test--TODO: implement
//#define ELECTRO_MAG     // uncomment if using PWM1 pin as a GPIO fired off of
                        // the pitch entry in the command signal

// DEFAULT CONTROLLER SETTINGS -- only important if flying in RC mode
#define MQXX          // load default control values for mini quads
//#define BQXX            // load default control values for buddy quads

#define SONAR_SPEED         9600        // serial speed for maxboxic sonar
#define LOGGING_RC_SPEED    115200      // serial speed for openlog data logger and spekrum rc receiver
#ifdef USE_UDP
    #define XBEE_SPEED          115200
#else
    #define XBEE_SPEED          56700       // serial speed for xbee wireless radio
#endif

// Clock speed
#define FCY 50000000

#define PI 3.14159265

// Type defs
#define uint8_t 	unsigned char
#define BYTE 		unsigned char
#define uint16_t 	unsigned int
#define int16_t		int
#define uint32_t	long unsigned int
#define int32_t		long int
#define int8_t		char			// 127 -> -128


// Pin defs
// LED pins
#define led_toggle(x)		((x) = !(x))
#ifdef V1_3
    #define LED_BLUE			LATBbits.LATB14
    #define LED_YELLOW			LATBbits.LATB13
    #define LED_GREEN			LATBbits.LATB12
    #define LED_RED			LATBbits.LATB11
#else
    #define LED_BLUE			LATEbits.LATE4
    #define LED_YELLOW			LATEbits.LATE3
    #define LED_GREEN			LATEbits.LATE2
    #define LED_RED			LATEbits.LATE1
#endif

#ifdef ELECTRO_MAG
    #define ELECTRO_MAG_PIN             LATDbits.LATD0
#endif

#define LED_OFF				1
#define LED_ON				0

#define DBG_PIN				LATAbits.LATA1 // debug pin - RA1

#define VBATT				ADC1BUF0	
#define VSCALE				6.654  // scale to get to mV
#define VBIAS				-11.78 // bias on voltage A2D -- numbers come from
					       // linear fit of test data
                                               // Data located in voltage cal google doc


// Servo defines
#define PWM1				OC1RS
#define PWM2				OC2RS
#define PWM3				OC3RS
#define PWM4				OC4RS
#define PWM5				OC5RS
#define PWM6				OC6RS
#define PWM7				OC7RS
#define PWM8				OC8RS
#define SETPWM(channel,x)	(channel = (x + 6250))

typedef struct sLoopFlags{
    uint8_t ReadGyro;
    uint8_t GyroProp;
    uint8_t AccMagCorrect;
    uint8_t ViconCorrect;
    uint8_t AttCtl;
    uint8_t SendSerial;
    uint8_t SendSensors;
    uint8_t ReadSerial;
    uint8_t StartTemperature;
    uint8_t ReadTemperature;
    uint8_t ReadPressure;
    uint8_t CanReadPressure;
    uint8_t StartPressure;
    uint8_t StartWait;
    uint8_t ToggleLED;
    uint8_t ReadAccMag;
    uint8_t I2C1Recover;
    uint8_t I2C2Recover;
    uint8_t LogData;
}tLoopFlags;


// Some helpful functions
void int16toQ16(_Q16 *to, int16_t *from);
_Q16 mult( _Q16 a, _Q16 b);
_Q16 _Q16sqrt(_Q16 in);
void _Q16sat(_Q16 * val, _Q16 high, _Q16 low); 
void uint8sat(uint8_t * val, uint8_t high, uint8_t low);
void int16sat(int16_t * val, int16_t high, int16_t low);
void Q16touint8(uint8_t *to, _Q16 *from);
void Q16toint16(int16_t *to, _Q16 *from);
void _Q16abs(_Q16 *val);

// C Prototype for function in div16.s
 _Q16 _IQ16div(_Q16, _Q16);

// Hardware initialization prototypes
void InitTimer1();
void InitTimer2();
void InitHardware();	
void InitIMU();
void InitPressure();
void InitPWM();
void InitA2D();
void InitCnsts();
void InitCnsts();

// Test Sensors
void SensorTest();

#endif

