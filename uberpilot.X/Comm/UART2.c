#include "Comm.h"
#include "p33Fxxxx.h"
#include "../defines.h"
#include "../AHRS/AHRS.h"

#include<stdlib.h>

// Globals
extern volatile tSensorCal SensorCal;
extern volatile tSensorData SensorData;
extern volatile tAHRSdata AHRSdata;
extern volatile tCmdData CmdData;
extern volatile tLoopFlags loop;
extern volatile tGains Gains;
extern volatile int16_t vbatt;
extern volatile tRCdata RCdata;
extern _Q16 num512, num2p0;

volatile unsigned int UART2txBuffA[128] __attribute__((space(dma)));
volatile unsigned int UART2txBuffB[128] __attribute__((space(dma)));
volatile unsigned int UART2txBuffSel = 0;
volatile unsigned int UART2txPtr = 0;
volatile unsigned int UART2txInProgress = 0;

volatile unsigned int UART2rxBuffA[128] __attribute__((space(dma)));
volatile unsigned int UART2rxBuffB[128] __attribute__((space(dma)));
volatile unsigned int UART2rxBuffSel = 0;

void UART2_Init(unsigned long int baud){

// Setup the UART
U2MODEbits.STSEL = 0; // 1 Stop bit
U2MODEbits.PDSEL = 0; // No Parity, 8 data bits
U2MODEbits.ABAUD = 0; // Auto-Baud Disabled
U2BRG = ((FCY/baud)/16) - 1;// BAUD Rate
U2STAbits.UTXISEL0 = 0; // Interrupt after one TX character is transmitted
U2STAbits.UTXISEL1 = 0;
U2STAbits.URXISEL = 0; // Interrupt after one RX character is received
U2MODEbits.UARTEN = 1; // Enable UART
U2STAbits.UTXEN = 1; // Enable UART TX
//IFS4bits.U2EIF = 0; // Clear UART2 error interrupt Flag
//IEC4bits.U2EIE = 1; // Enable UART2 error interrupt 

// Setup DMA for TX	
DMA0CON = 0x2001; // One-Shot, Post-Increment, RAM-to-Peripheral
DMA0CNT = 7; // 8 DMA requests
DMA0REQ = 0x001F; // Select UART2 Transmitter
DMA0PAD = (volatile unsigned int) &U2TXREG;
IFS0bits.DMA0IF = 0; // Clear DMA Interrupt Flag
IEC0bits.DMA0IE = 1; // Enable DMA Interrupt

// Setup DMA for RX
DMA1CONbits.SIZE = 0; // Word transfers
DMA1CONbits.DIR = 0; // Peripheral to memory
DMA1CONbits.MODE = 0b01; // One-shot no ping pong
DMA1CNT = 254; // 255 DMA requests max
DMA1REQ = 0x001E; // Select UART2 Receiver
DMA1PAD = (volatile unsigned int) &U2RXREG;
DMA1STA = __builtin_dmaoffset(UART2rxBuffA);
IFS0bits.DMA1IF = 0; // Clear DMA interrupt
IEC0bits.DMA1IE = 1; // Enable DMA interrupt
DMA1CONbits.CHEN = 1; // Enable DMA Channel*/

}

void UART2_SendSensorData(void){
    //UART2_StuffPacket(PACKETID_SENSOR, sizeof(tSensorData), &SensorData);
}

void UART2_SendAHRSData(void){
    tAHRSpacket pkt;
    pkt.qo_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.o )  );
    pkt.qx_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.x )  );
    pkt.qy_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.y )  );
    pkt.qz_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.z )  );

    pkt.qo_meas = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.o )  );
    pkt.qx_meas = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.x )  );
    pkt.qy_meas = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.y )  );
    pkt.qz_meas = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.z )  );

    pkt.p = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.p )  );
    pkt.q = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.q )  );
    pkt.r = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.r )  );

    UART2_StuffPacket(PACKETID_AHRS, sizeof(tAHRSpacket), &pkt);
}

void UART2_SendVoltage(void){
    tVoltagePacket pkt;
    pkt.voltage = vbatt;

    UART2_StuffPacket(PACKETID_VOLTAGE, sizeof(tVoltagePacket), &pkt);
}

void UART2_SendHealth(void){
    tHealthPacket pkt;
    pkt.current1 = SensorData.current1;
    pkt.current2 = SensorData.current2;
    pkt.current3 = SensorData.current3;
    pkt.current4 = SensorData.current4;
    pkt.temp1 = SensorData.temp1;
    pkt.temp2 = SensorData.temp2;
    pkt.temp3 = SensorData.temp3;
    pkt.temp4 = SensorData.temp4;

    UART2_StuffPacket(PACKETID_HEALTH, sizeof(tHealthPacket), &pkt);
}

void UART2_LogAHRS(void){

    tLogPacket pkt;
    pkt.qo_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.o )  );
    pkt.qx_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.x )  );
    pkt.qy_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.y )  );
    pkt.qz_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.z )  );
    pkt.p_est = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.p )  );
    pkt.q_est = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.q )  );
    pkt.r_est = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.r )  );

    pkt.qo_cmd = (int16_t)( QUAT_SCALE * _itofQ16( CmdData.q_cmd.o )  );
    pkt.qx_cmd = (int16_t)( QUAT_SCALE * _itofQ16( CmdData.q_cmd.x )  );
    pkt.qy_cmd = (int16_t)( QUAT_SCALE * _itofQ16( CmdData.q_cmd.y )  );
    pkt.qz_cmd = (int16_t)( QUAT_SCALE * _itofQ16( CmdData.q_cmd.z )  );
    pkt.p_cmd = (int16_t)( ROT_SCALE * _itofQ16( CmdData.p )  );
    pkt.q_cmd = (int16_t)( ROT_SCALE * _itofQ16( CmdData.q )  );
    pkt.r_cmd = (int16_t)( ROT_SCALE * _itofQ16( CmdData.r )  );

    pkt.throttle = CmdData.throttle;
    pkt.collective = CmdData.collective;

    UART2_StuffPacket(PACKETID_LOG, sizeof(tLogPacket), &pkt);
}

void UART2_StuffPacket(BYTE packetId, BYTE len, BYTE* data)
{
    // Start Checksum
    BYTE chksum = 0;
    BYTE ptr = 0;
    BYTE i;
    BYTE TxBuff[256];

    if (len > 240)
        return;

    // STX
    TxBuff[ptr++] = STXA;
    TxBuff[ptr++] = STXB;

    // Paket Id
    TxBuff[ptr++] = packetId;
    chksum += packetId;

    // Paket Data Length
    TxBuff[ptr++] = len;
    chksum += len;

    // Stuff Data
    for(i = 0; i < len; i++)
    {
        // Send Byte
        TxBuff[ptr++] = data[i];
        chksum += data[i];
    }

    // Checksum Calculate and Send
    chksum ^= 0xFF;
    chksum += 1;
    TxBuff[ptr++] = chksum;

    UART2_PutData(TxBuff, ptr);
}
//======================================================================

void UART2_PutData(unsigned char * data, unsigned int size){
    // Select the active DMA TX buffer
    unsigned int * activeBuff;
    if(UART2txBuffSel == 0){
        activeBuff = UART2txBuffA;
    }else{
        activeBuff = UART2txBuffB;
    }

    // TODO: Make sure there is no overflow
    // Copy data into the DMA TX buffer
    int i =0;
    for(i=0; i <size; i++)
    {
        activeBuff[UART2txPtr] = data[i];
        UART2txPtr++;
    }
}

/*---------------------------------------------------------------------
  Function Name: UART_ParsePacket
  Description:   Parses and rescales a good data packet
  Inputs:        Pointer to the received data packet to be parsed
  Returns:       None
-----------------------------------------------------------------------*/
tSensorCalPacket * sensorCalPacket;
tCmdPacket * cmdPacket;
tGainsPacket * gainsPacket;
void UART_ParsePacket(struct strPacket * rxPacket){
    switch(rxPacket->id){
    case PACKETID_SENSORCAL:
        sensorCalPacket = (tSensorCalPacket*) rxPacket->data;

        // scaled by 1000000 (divide by 10000 and then by 100)
        int16_t tmp = QUAT_SCALE;
        int16_t tmp2 = ROT_SCALE;
        _Q16 div = 0;
        _Q16 div2 = 0;
        int16toQ16(&div, &tmp); int16toQ16(&div2, &tmp2);
        int16toQ16(&SensorCal.gyroScale, &sensorCalPacket->gyroScale); SensorCal.gyroScale = _IQ16div(_IQ16div(SensorCal.gyroScale,div), div2);
        int16toQ16(&SensorCal.K_AttFilter, &sensorCalPacket->K_AttFilter); SensorCal.K_AttFilter = _IQ16div(_IQ16div(SensorCal.K_AttFilter,div), div2);
        int16toQ16(&SensorCal.kGyroBias, &sensorCalPacket->kGyroBias); SensorCal.kGyroBias = _IQ16div(_IQ16div(SensorCal.kGyroBias,div), div2);

        break;
    case PACKETID_CMD:

        cmdPacket = (tCmdPacket*) rxPacket->data;

        // quaternions are scaled by 10000 when sent over (16 bit signed ints)
        tmp = QUAT_SCALE;
        int16toQ16(&div, &tmp);

        // convert vicon quaterion to type Q16, then divide by 10000 to get into correct scaling
        int16toQ16(&AHRSdata.q_meas.o, &cmdPacket->qo_meas); AHRSdata.q_meas.o = _IQ16div(AHRSdata.q_meas.o, div);
        int16toQ16(&AHRSdata.q_meas.x, &cmdPacket->qx_meas); AHRSdata.q_meas.x = _IQ16div(AHRSdata.q_meas.x, div);
        int16toQ16(&AHRSdata.q_meas.y, &cmdPacket->qy_meas); AHRSdata.q_meas.y = _IQ16div(AHRSdata.q_meas.y, div);
        int16toQ16(&AHRSdata.q_meas.z, &cmdPacket->qz_meas); AHRSdata.q_meas.z = _IQ16div(AHRSdata.q_meas.z, div);
        loop.ViconCorrect = 1; // use the new vicon attitude data to update the attitude estimate

        // convert desired quaterion to type Q16, then divide by 10000 to get into correct scaling
        int16toQ16(&CmdData.q_cmd.o, &cmdPacket->qo_cmd); CmdData.q_cmd.o = _IQ16div(CmdData.q_cmd.o, div);
        int16toQ16(&CmdData.q_cmd.x, &cmdPacket->qx_cmd); CmdData.q_cmd.x = _IQ16div(CmdData.q_cmd.x, div);
        int16toQ16(&CmdData.q_cmd.y, &cmdPacket->qy_cmd); CmdData.q_cmd.y = _IQ16div(CmdData.q_cmd.y, div);
        int16toQ16(&CmdData.q_cmd.z, &cmdPacket->qz_cmd); CmdData.q_cmd.z = _IQ16div(CmdData.q_cmd.z, div);

        // p,q,r are scaled by 100
        tmp = ROT_SCALE;
        int16toQ16(&div, &tmp);
        int16toQ16(&CmdData.p, &cmdPacket->p_cmd); CmdData.p = _IQ16div(CmdData.p, div);
        int16toQ16(&CmdData.q, &cmdPacket->q_cmd); CmdData.q = _IQ16div(CmdData.q, div);
        int16toQ16(&CmdData.r, &cmdPacket->r_cmd); CmdData.r = _IQ16div(CmdData.r, div);

        CmdData.throttle = cmdPacket->throttle;
        CmdData.collective = cmdPacket->collective;
        CmdData.AttCmd = cmdPacket->AttCmd;

#ifdef ELECTRO_MAG
        if (CmdData.collective == 1) {
            // turn electro magnet on
            ELECTRO_MAG_PIN = 1;
        } else {
            ELECTRO_MAG_PIN = 0;
        }
#endif

        /*if (CmdData.AttCmd != 0)
        {
            // convert vicon quaterion to type Q16, then divide by 10000 to get into correct scaling
            int16toQ16(&AHRSdata.q_meas.o, &cmdPacket->qo_meas); AHRSdata.q_meas.o = _IQ16div(AHRSdata.q_meas.o, div);
            int16toQ16(&AHRSdata.q_meas.x, &cmdPacket->qx_meas); AHRSdata.q_meas.x = _IQ16div(AHRSdata.q_meas.x, div);
            int16toQ16(&AHRSdata.q_meas.y, &cmdPacket->qy_meas); AHRSdata.q_meas.y = _IQ16div(AHRSdata.q_meas.y, div);
            int16toQ16(&AHRSdata.q_meas.z, &cmdPacket->qz_meas); AHRSdata.q_meas.z = _IQ16div(AHRSdata.q_meas.z, div);
            loop.ViconCorrect = 1; // use the new vicon attitude data to update the attitude estimate
        }
        led_toggle(LED_BLUE);*/
        break;

    case PACKETID_GAINS:
        gainsPacket = (tGainsPacket*) rxPacket->data;

        // gains are scaled by 10
        tmp = GAINS_SCALE;
        int16toQ16(&div, &tmp);
        int16toQ16(&Gains.Kp_roll, &gainsPacket->Kp_roll); Gains.Kp_roll = _IQ16div(Gains.Kp_roll, div);
        int16toQ16(&Gains.Kd_roll, &gainsPacket->Kd_roll); Gains.Kd_roll = _IQ16div(Gains.Kd_roll, div);
        int16toQ16(&Gains.Ki_roll, &gainsPacket->Ki_roll); Gains.Ki_roll = _IQ16div(Gains.Ki_roll, div);

        int16toQ16(&Gains.Kp_pitch, &gainsPacket->Kp_pitch); Gains.Kp_pitch = _IQ16div(Gains.Kp_pitch, div);
        int16toQ16(&Gains.Kd_pitch, &gainsPacket->Kd_pitch); Gains.Kd_pitch = _IQ16div(Gains.Kd_pitch, div);
        int16toQ16(&Gains.Ki_pitch, &gainsPacket->Ki_pitch); Gains.Ki_pitch = _IQ16div(Gains.Ki_pitch, div);

        int16toQ16(&Gains.Kp_yaw, &gainsPacket->Kp_yaw); Gains.Kp_yaw = _IQ16div(Gains.Kp_yaw, div);
        int16toQ16(&Gains.Kd_yaw, &gainsPacket->Kd_yaw); Gains.Kd_yaw = _IQ16div(Gains.Kd_yaw, div);
        int16toQ16(&Gains.Ki_yaw, &gainsPacket->Ki_yaw); Gains.Ki_yaw = _IQ16div(Gains.Ki_yaw, div);

        int16toQ16(&Gains.PWM1_trim, &gainsPacket->Servo1_trim);
        int16toQ16(&Gains.PWM2_trim, &gainsPacket->Servo2_trim);
        int16toQ16(&Gains.PWM3_trim, &gainsPacket->Servo3_trim);
        int16toQ16(&Gains.PWM4_trim, &gainsPacket->Servo4_trim);

        int16toQ16(&Gains.maxang, &gainsPacket->maxang);  Gains.maxang = _IQ16div(Gains.maxang, div);
        int16toQ16(&Gains.motorFF, &gainsPacket->motorFF); Gains.motorFF = _IQ16div(_IQ16div(Gains.motorFF, div),div);
        Gains.lowBatt = gainsPacket->lowBatt;
        Gains.stream_data = gainsPacket->stream_data;

        break;

    case PACKETID_SONAR:
        SensorData.sonarZ = atoi(rxPacket->data);
        break;

    default:
        break;
    }
}

uint8_t rxState_uart2;
uint8_t rxChkSum_uart2;
uint8_t rxCount_uart2;
struct strPacket rxPacket_uart2;
void UART2_FlushRX(void){
    int len;
    uint8_t * buff;

    if(UART2rxBuffSel == 0){
        // Get the number of bytes received so far
        len = (DMA1STA - __builtin_dmaoffset(UART2rxBuffA)) / 2;
        if(len == 0){
            if( U2STAbits.OERR ){
                U2STAbits.OERR    = 0; // clear overflow flag if set
                rxState_uart2 = RXSTATE_STXA; //reset parse packet state
            }
            // No data rec'd, exit
            return;
        }
        // Disable DMA
        DMA1CONbits.CHEN = 0;

        // Switch the buffer, Enable DMA
        UART2rxBuffSel = 1;
        DMA1STA = __builtin_dmaoffset(UART2rxBuffB);
        DMA1CONbits.CHEN = 1;

        // Parse the received data
        buff = (uint8_t *)UART2rxBuffA;
    }else{
        // Get the number of bytes received so far
        len = (DMA1STA - __builtin_dmaoffset(UART2rxBuffB)) / 2;
        if(len == 0){
            if( U2STAbits.OERR ){
                U2STAbits.OERR    = 0; // clear overflow flag if set
                rxState_uart2 = RXSTATE_STXA; //reset parse packet state
            }
            // No data rec'd, exit
            return;
        }
        // Disable DMA
        DMA1CONbits.CHEN = 0;

        // Switch the buffer, Enable DMA
        UART2rxBuffSel = 0;
        DMA1STA = __builtin_dmaoffset(UART2rxBuffA);
        DMA1CONbits.CHEN = 1;

        // Parse the received data
        buff = (uint8_t *)UART2rxBuffB;

    }

    #ifdef V1_3

        // Process Data in Buffer
        int i = 0;
        uint8_t b, channel;
        int16_t data;
        _Q16 tmpQ16;
        while (i < len)
        {
            // Get the next byte
            b = buff[i*2];


            // PROCESS Spektrum receiver data -- see http://www.desertrc.com/spektrum_protocol.htm for protocol
            // Determine what to do with received character
            switch(rxState_uart2){
                case RXSTATE_STXA: // first byte is 0x03
                    if (b == RCXA)
                        rxState_uart2 = RXSTATE_STXB;
                    i++;
                    break;
                case RXSTATE_STXB:
                    if (b == RCXA)
                        rxState_uart2 = RXSTATE_STXB;
                    else if (b == RCXB)
                        rxState_uart2 = RXSTATE_DATA;
                    else
                        rxState_uart2 = RXSTATE_STXA;
                    i++;
                    break;
                case RXSTATE_DATA:
                    // TODO- implement being able to process this faster (hold state)
                    if ((len-i) > 14) // fourteen more bytes
                    {
                        int j = i + 14;
                        while (i < j)
                        {
                            uint16_t rcdata = buff[i*2] << 8; // MSB
                            rcdata += buff[(i+1)*2];	// LSB
                            channel = rcdata >> 10; // get 6 first bits
                            data = (int16_t) (rcdata & 0b0000001111111111); // get last 10 bits
                            i += 2;
                            switch(channel){	// process channel data
                                case 0:
                                    RCdata.ch0 = data;
                                    break;
                                case 1:
                                    int16toQ16(&tmpQ16,&data);
                                    tmpQ16 -= num512;
                                    RCdata.ch1 = _IQ16div(tmpQ16,num512);
                                    break;
                                case 2:
                                    int16toQ16(&tmpQ16,&data);
                                    tmpQ16 -= num512;
                                    RCdata.ch2 = _IQ16div(tmpQ16,num512);
                                    break;
                                case 3:
                                    int16toQ16(&tmpQ16,&data);
                                    tmpQ16 -= num512;
                                    RCdata.ch3 = _IQ16div(tmpQ16,num512);
                                    break;
                                case 4:
                                    RCdata.ch4 = data;
                                    break;
                                case 5:
                                    RCdata.ch5 = data;
                                    break;
                                case 6:
                                    RCdata.ch6 = data;
                                    break;
                                default:
                                    break;
                            }
                        }
                        // manual mode
                        if (RCdata.ch5 > 600) {
                            CmdData.AttCmd = 1;
                            CmdData.throttle = RCdata.ch0/4;  // RCdata is between 0 and 1023, this will be approximately between 0 and 255
                            _Q16 halfRoll = -RCdata.ch1; //mult(RCdata.ch1,num0p5);  // this is about -0.6 -> 0.6  which is +/- ~70 degrees
                            _Q16 halfPitch = RCdata.ch2; //mult(RCdata.ch2,num0p5);
                            _Q16 cosRoll = _Q16cos(halfRoll);
                            _Q16 sinRoll = _Q16sin(halfRoll);
                            _Q16 cosPitch = _Q16cos(halfPitch);
                            _Q16 sinPitch = _Q16sin(halfPitch);
                            CmdData.q_cmd.o = mult(cosRoll,cosPitch);
                            CmdData.q_cmd.x = mult(sinRoll,cosPitch);
                            CmdData.q_cmd.y = mult(cosRoll,sinPitch);
                            CmdData.q_cmd.z = -mult(sinRoll,sinPitch);
                            CmdData.p = CmdData.q = 0;
                            CmdData.r = mult(RCdata.ch3,num2p0);
#ifdef BQXX
                            if (CmdData.throttle < 42)
                                CmdData.throttle = 0;
                            else
                                CmdData.throttle -= 42;
#endif
                        } else
                        {
                            CmdData.AttCmd = 0;
                        }

                        // If we are getting RC data, we should correct the gyros to zero
                        //loop.AccMagCorrect = 1;
                        loop.ViconCorrect = 1;
                    }
                    rxState_uart2 = RXSTATE_STXA;

                    break;

                default:
                    break;

            }
        }
    #else
        // Process Data in Buffer
        int i;
        uint8_t b;
        for(i=0;i<len;i++)
        {
            // Get the next byte
            b = buff[i*2];

            // Determine what to do with received character
            switch(rxState_uart2){
                case RXSTATE_STXA:
                    if (b == STXA) rxState_uart2 = RXSTATE_STXB;
                    break;
                case RXSTATE_STXB:
                    if (b == STXA)
                            rxState_uart2 = RXSTATE_STXB;
                    else if (b == STXB)
                            rxState_uart2 = RXSTATE_PACKETID;
                    else
                            rxState_uart2 = RXSTATE_STXA;
                    break;
                case RXSTATE_PACKETID:
                    rxPacket_uart2.id = b;
                    rxState_uart2 = RXSTATE_LEN;
                    rxChkSum_uart2 = b;
                    break;
                case RXSTATE_LEN:
                    rxPacket_uart2.len = b;
                    rxState_uart2 = RXSTATE_DATA;
                    rxCount_uart2 = 0;
                    rxChkSum_uart2 += b;
                    break;
                case RXSTATE_DATA:
                    rxPacket_uart2.data[rxCount_uart2] = b;
                    rxCount_uart2++;
                    if (rxCount_uart2 == rxPacket_uart2.len) rxState_uart2 = RXSTATE_CHKSUM;
                    rxChkSum_uart2 += b;
                    break;
                case RXSTATE_CHKSUM:
                    rxState_uart2 = RXSTATE_STXA;
                    rxChkSum_uart2 += b;
                    if (rxChkSum_uart2 == 0) {
                        // Good packet received, execute it
                        UART_ParsePacket(&rxPacket_uart2);
                    }
                    else
                        Nop();
                    break;
                default:
                        break;
            }
        }

    #endif
}

void UART2_FlushTX(void){
    // Transfer in progress?
    if(UART2txInProgress)
        return;

    // Are there bytes to send?
    if(UART2txPtr == 0)
        return;

    DMA0CNT = UART2txPtr - 1; // Set the number of transfers
    if(UART2txBuffSel == 0){
        DMA0STA = __builtin_dmaoffset(UART2txBuffA);
        UART2txBuffSel = 1;
    }else{
        DMA0STA = __builtin_dmaoffset(UART2txBuffB);
        UART2txBuffSel = 0;
    }

    UART2txPtr = 0;
    UART2txInProgress = 1;
    DMA0CONbits.CHEN = 1;// Enable DMA0 Channel
    DMA0REQbits.FORCE = 1;// Manual mode: Kick-start the 1st transfer
}

// DMA TX interrupt handler
void __attribute__((__interrupt__)) _DMA0Interrupt(void)
{
    UART2txInProgress = 0;
    IFS0bits.DMA0IF = 0;// Clear the DMA0 Interrupt Flag;
}
