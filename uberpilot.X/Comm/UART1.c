#include "Comm.h"
#include "p33Fxxxx.h"
#include "../defines.h"
#include "../AHRS/AHRS.h"

// Globals
extern volatile tSensorData SensorData;
extern volatile tSensorCal SensorCal;
extern volatile tAHRSdata AHRSdata;
extern volatile tCmdData CmdData;
extern volatile tLoopFlags loop;
extern volatile tRCdata RCdata;
extern volatile int16_t vbatt;
extern _Q16 num512, num2p0;

volatile unsigned int UART1txBuffA[128] __attribute__((space(dma)));
volatile unsigned int UART1txBuffB[128] __attribute__((space(dma)));
volatile unsigned int UART1txBuffSel = 0;
volatile unsigned int UART1txPtr = 0;
volatile unsigned int UART1txInProgress = 0;

volatile unsigned int UART1rxBuffA[128] __attribute__((space(dma)));
volatile unsigned int UART1rxBuffB[128] __attribute__((space(dma)));
volatile unsigned int UART1rxBuffSel = 0;

void UART1_Init(unsigned long int baud){

// Setup the UART
U1MODEbits.STSEL = 0; // 1 Stop bit
U1MODEbits.PDSEL = 0; // No Parity, 8 data bits
U1MODEbits.ABAUD = 0; // Auto-Baud Disabled
U1BRG = ((FCY/baud)/16) - 1;// BAUD Rate
U1STAbits.UTXISEL0 = 0; // Interrupt after one TX character is transmitted
U1STAbits.UTXISEL1 = 0;
U1STAbits.URXISEL = 0; // Interrupt after one RX character is received
U1MODEbits.UARTEN = 1; // Enable UART
U1STAbits.UTXEN = 1; // Enable UART TX

// Setup DMA for TX
DMA3CON = 0x2001; // One-Shot, Post-Increment, RAM-to-Peripheral
DMA3CNT = 7; // 8 DMA requests
DMA3REQ = 0x000C; // Select UART1 Transmitter
DMA3PAD = (volatile unsigned int) &U1TXREG;
IFS2bits.DMA3IF = 0; // Clear DMA Interrupt Flag
IEC2bits.DMA3IE = 1; // Enable DMA Interrupt

// Setup DMA for RX
DMA4CONbits.SIZE = 0; // Word transfers
DMA4CONbits.DIR = 0; // Peripheral to memory
DMA4CONbits.MODE = 0b01; // One-shot no ping pong
DMA4CNT = 254; // 255 DMA requests max
DMA4REQ = 0x000B; // Select UART1 Receiver
DMA4PAD = (volatile unsigned int) &U1RXREG;
DMA4STA = __builtin_dmaoffset(UART1rxBuffA);
IFS2bits.DMA4IF = 0; // Clear DMA interrupt
IEC2bits.DMA4IE = 1; // Enable DMA interrupt
DMA4CONbits.CHEN = 1; // Enable DMA Channel*/

}

void UART1_SendAHRSData(void){
    tAHRSpacket pkt;
    pkt.qo_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.o )  );
    pkt.qx_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.x )  );
    pkt.qy_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.y )  );
    pkt.qz_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.z )  );

    pkt.qo_meas = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.o )  );
    pkt.qx_meas = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.x )  );
    pkt.qy_meas = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.y )  );
    pkt.qz_meas = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.z )  );

    pkt.p = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.p )  );
    pkt.q = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.q )  );
    pkt.r = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.r )  );

    UART1_StuffPacket(PACKETID_AHRS, sizeof(tAHRSpacket), &pkt);
}

void UART1_SendVoltage(void){
    tVoltagePacket pkt;
    pkt.voltage = vbatt;

    UART1_StuffPacket(PACKETID_VOLTAGE, sizeof(tVoltagePacket), &pkt);
}

void UART1_SendHealth(void){
    tHealthPacket pkt;
    pkt.current1 = SensorData.current1;
    pkt.current2 = SensorData.current2;
    pkt.current3 = SensorData.current3;
    pkt.current4 = SensorData.current4;
    pkt.temp1 = SensorData.temp1;
    pkt.temp2 = SensorData.temp2;
    pkt.temp3 = SensorData.temp3;
    pkt.temp4 = SensorData.temp4;

    UART1_StuffPacket(PACKETID_HEALTH, sizeof(tHealthPacket), &pkt);
}

void UART1_SendSensors(void){
    tSensorsPacket pkt;
    pkt.gyroX = SensorData.gyroX;
    pkt.gyroY = SensorData.gyroY;
    pkt.gyroZ = SensorData.gyroZ;
    pkt.accelX = SensorData.accX;
    pkt.accelY = SensorData.accY;
    pkt.accelZ = SensorData.accZ;
    pkt.magX = SensorData.magX;
    pkt.magY = SensorData.magY;
    pkt.magZ = SensorData.magZ;
    pkt.sonarZ = SensorData.sonarZ;

    UART1_StuffPacket(PACKETID_SENSORS, sizeof(tSensorsPacket), &pkt);
}

void UART1_SendAltitude(void){
    tAltitudePacket pkt;
    pkt.sonarZ = SensorData.sonarZ;
    pkt.pressure = SensorData.pressure_filt;
    pkt.temperature = SensorData.temperature_filt;//SensorData.temperature_filt;
    pkt.accelX = (int16_t)( 1000 * _itofQ16( AHRSdata.ax )  );
    pkt.accelY = (int16_t)( 1000 * _itofQ16( AHRSdata.ay )  );
    pkt.accelZ = (int16_t)( 1000 * _itofQ16( AHRSdata.az )  );

    UART1_StuffPacket(PACKETID_ALTITUDE, sizeof(tAltitudePacket), &pkt);
}

void UART1_LogAHRS(void){
    /*static int cnt = 0;
    BYTE TxBuff[256];
    BYTE ptr = sprintf(TxBuff,"%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%d,%d,%d\r\n",cnt,
            _itofQ16(AHRSdata.p),_itofQ16(AHRSdata.q),_itofQ16(AHRSdata.r),
            _itofQ16(AHRSdata.q_est.o),_itofQ16(AHRSdata.q_est.x),_itofQ16(AHRSdata.q_est.y),_itofQ16(AHRSdata.q_est.z),
            _itofQ16(CmdData.p),_itofQ16(CmdData.q),_itofQ16(CmdData.r),
            _itofQ16(CmdData.q_cmd.o),_itofQ16(CmdData.q_cmd.x),_itofQ16(CmdData.q_cmd.y),_itofQ16(CmdData.q_cmd.z),
            CmdData.AttCmd,CmdData.throttle,
            _itofQ16(SendCmdData.m1),_itofQ16(SendCmdData.m2),_itofQ16(SendCmdData.m3),_itofQ16(SendCmdData.m4));
    cnt++;

    UART1_PutData(TxBuff,ptr);*/

    tLogPacket pkt;
    pkt.qo_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.o )  );
    pkt.qx_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.x )  );
    pkt.qy_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.y )  );
    pkt.qz_est = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_est.z )  );
    pkt.p_est = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.p )  );
    pkt.q_est = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.q )  );
    pkt.r_est = (int16_t)( ROT_SCALE * _itofQ16( AHRSdata.r )  );

    pkt.qo_cmd = (int16_t)( QUAT_SCALE * _itofQ16( CmdData.q_cmd.o )  );
    pkt.qx_cmd = (int16_t)( QUAT_SCALE * _itofQ16( CmdData.q_cmd.x )  );
    pkt.qy_cmd = (int16_t)( QUAT_SCALE * _itofQ16( CmdData.q_cmd.y )  );
    pkt.qz_cmd = (int16_t)( QUAT_SCALE * _itofQ16( CmdData.q_cmd.z )  );
    pkt.p_cmd = (int16_t)( ROT_SCALE * _itofQ16( CmdData.p )  );
    pkt.q_cmd = (int16_t)( ROT_SCALE * _itofQ16( CmdData.q )  );
    pkt.r_cmd = (int16_t)( ROT_SCALE * _itofQ16( CmdData.r )  );

//    pkt.qo_cmd = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.o )  );
//    pkt.qx_cmd = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.x )  );
//    pkt.qy_cmd = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.y )  );
//    pkt.qz_cmd = (int16_t)( QUAT_SCALE * _itofQ16( AHRSdata.q_meas.z )  );
//    pkt.p_cmd = (int16_t)( ROT_SCALE * _itofQ16( SensorCal.pBias )  );
//    pkt.q_cmd = (int16_t)( ROT_SCALE * _itofQ16( SensorCal.qBias )  );
//    pkt.r_cmd = (int16_t)( ROT_SCALE * _itofQ16( SensorCal.rBias )  );

    pkt.throttle = CmdData.throttle;
    pkt.collective = CmdData.collective;

    UART1_StuffPacket(PACKETID_LOG, sizeof(tLogPacket), &pkt);
}

void UART1_StuffPacket(BYTE packetId, BYTE len, BYTE* data)
{
    // Start Checksum
    BYTE chksum = 0;
    BYTE ptr = 0;
    BYTE i;
    BYTE TxBuff[256];

    if (len > 240)
        return;

    // STX
    TxBuff[ptr++] = STXA;
    TxBuff[ptr++] = STXB;

    // Paket Id
    TxBuff[ptr++] = packetId;
    chksum += packetId;

    // Paket Data Length
    TxBuff[ptr++] = len;
    chksum += len;

    // Stuff Data
    for(i = 0; i < len; i++)
    {
        // Send Byte
        TxBuff[ptr++] = data[i];
        chksum += data[i];
    }

    // Checksum Calculate and Send
    chksum ^= 0xFF;
    chksum += 1;
    TxBuff[ptr++] = chksum;

    UART1_PutData(TxBuff, ptr);
}
//======================================================================

void UART1_PutData(unsigned char * data, unsigned int size){
    // Select the active DMA TX buffer
    unsigned int * activeBuff;
    if(UART1txBuffSel == 0){
        activeBuff = UART1txBuffA;
    }else{
        activeBuff = UART1txBuffB;
    }

    // TODO: Make sure there is no overflow
    // Copy data into the DMA TX buffer
    int i =0;

    for(i=0; i <size; i++)
    {
        activeBuff[UART1txPtr] = data[i];
        UART1txPtr++;
    }
}

uint8_t rxState_uart1;
uint8_t rxChkSum_uart1;
uint8_t rxCount_uart1;
struct strPacket rxPacket_uart1;
void UART1_FlushRX(void){
    int len;
    uint8_t * buff;

    if(UART1rxBuffSel == 0){
        // Get the number of bytes received so far
        len = (DMA4STA - __builtin_dmaoffset(UART1rxBuffA)) / 2;
        if(len == 0){
            if( U1STAbits.OERR ){
                U1STAbits.OERR    = 0; // clear overflow flag if set
                rxState_uart1 = RXSTATE_STXA; //reset parse packet state
            }
            // No data rec'd, exit
            return;
        }
        // Disable DMA
        DMA4CONbits.CHEN = 0;

        // Switch the buffer, Enable DMA
        UART1rxBuffSel = 1;
        DMA4STA = __builtin_dmaoffset(UART1rxBuffB);
        DMA4CONbits.CHEN = 1;

        // Parse the received data
        buff = (uint8_t *)UART1rxBuffA;
    }else{
        // Get the number of bytes received so far
        len = (DMA4STA - __builtin_dmaoffset(UART1rxBuffB)) / 2;
        if(len == 0){
            if( U1STAbits.OERR ){
                U1STAbits.OERR    = 0; // clear overflow flag if set
                rxState_uart1 = RXSTATE_STXA; //reset parse packet state
            }
            // No data rec'd, exit
            return;
        }
        // Disable DMA
        DMA4CONbits.CHEN = 0;

        // Switch the buffer, Enable DMA
        UART1rxBuffSel = 0;
        DMA4STA = __builtin_dmaoffset(UART1rxBuffA);
        DMA4CONbits.CHEN = 1;

        // Parse the received data
        buff = (uint8_t *)UART1rxBuffB;

    }

    #ifdef V1_3
        // Process Data in Buffer
        int i;
        uint8_t b;
        for(i=0;i<len;i++)
        {
            // Get the next byte
            b = buff[i*2];

            // Determine what to do with received character
            switch(rxState_uart1){
                case RXSTATE_STXA:
                    if (b == STXA) rxState_uart1 = RXSTATE_STXB;
                    break;
                case RXSTATE_STXB:
                    if (b == STXA)
                            rxState_uart1 = RXSTATE_STXB;
                    else if (b == STXB)
                            rxState_uart1 = RXSTATE_PACKETID;
                    else
                            rxState_uart1 = RXSTATE_STXA;
                    break;
                case RXSTATE_PACKETID:
                    rxPacket_uart1.id = b;
                    rxState_uart1 = RXSTATE_LEN;
                    rxChkSum_uart1 = b;
                    break;
                case RXSTATE_LEN:
                    rxPacket_uart1.len = b;
                    rxState_uart1 = RXSTATE_DATA;
                    rxCount_uart1 = 0;
                    rxChkSum_uart1 += b;
                    break;
                case RXSTATE_DATA:
                    rxPacket_uart1.data[rxCount_uart1] = b;
                    rxCount_uart1++;
                    if (rxCount_uart1 == rxPacket_uart1.len) rxState_uart1 = RXSTATE_CHKSUM;
                    rxChkSum_uart1 += b;
                    break;
                case RXSTATE_CHKSUM:
                    rxState_uart1 = RXSTATE_STXA;
                    rxChkSum_uart1 += b;
                    if (rxChkSum_uart1 == 0) {
                        // Good packet received, execute it
                        UART_ParsePacket(&rxPacket_uart1);
                    }
                    else
                        Nop();
                    break;
                default:
                        break;
            }
        }
    #else
        // Process Data in Buffer
        int i = 0;
        uint8_t b, channel;
        int16_t data;
        _Q16 tmpQ16;
        while (i < len)
        {
            // Get the next byte
            b = buff[i*2];


            // PROCESS Spektrum receiver data -- see http://www.desertrc.com/spektrum_protocol.htm for protocol
            // Determine what to do with received character
            switch(rxState_uart1){
                case RXSTATE_STXA: // first byte is 0x03
                    if (b == RCXA)
                        rxState_uart1 = RXSTATE_STXB;
                    i++;
                    break;
                case RXSTATE_STXB:
                    if (b == RCXA)
                        rxState_uart1 = RXSTATE_STXB;
                    else if (b == RCXB)
                        rxState_uart1 = RXSTATE_DATA;
                    else
                        rxState_uart1 = RXSTATE_STXA;
                    i++;
                    break;
                case RXSTATE_DATA:
                    // TODO- implement being able to process this faster (hold state)
                    if ((len-i) > 14) // fourteen more bytes
                    {
                        int j = i + 14;
                        while (i < j)
                        {
                            uint16_t rcdata = buff[i*2] << 8; // MSB
                            rcdata += buff[(i+1)*2];	// LSB
                            channel = rcdata >> 10; // get 6 first bits
                            data = (int16_t) (rcdata & 0b0000001111111111); // get last 10 bits
                            i += 2;
                            switch(channel){	// process channel data
                                case 0:
                                    RCdata.ch0 = data;
                                    break;
                                case 1:
                                    int16toQ16(&tmpQ16,&data);
                                    tmpQ16 -= num512;
                                    RCdata.ch1 = _IQ16div(tmpQ16,num512);
                                    break;
                                case 2:
                                    int16toQ16(&tmpQ16,&data);
                                    tmpQ16 -= num512;
                                    RCdata.ch2 = _IQ16div(tmpQ16,num512);
                                    break;
                                case 3:
                                    int16toQ16(&tmpQ16,&data);
                                    tmpQ16 -= num512;
                                    RCdata.ch3 = _IQ16div(tmpQ16,num512);
                                    break;
                                case 4:
                                    RCdata.ch4 = data;
                                    break;
                                case 5:
                                    RCdata.ch5 = data;
                                    break;
                                case 6:
                                    RCdata.ch6 = data;
                                    break;
                                default:
                                    break;
                            }
                        }
                        // manual mode
                        if (RCdata.ch5 > 600) {
                            CmdData.AttCmd = 1;
                            CmdData.throttle = RCdata.ch0/4;  // RCdata is between 0 and 1023, this will be approximately between 0 and 255
                            _Q16 halfRoll = -RCdata.ch1; //mult(RCdata.ch1,num0p5);  // this is about -0.6 -> 0.6  which is +/- ~70 degrees
                            _Q16 halfPitch = RCdata.ch2; //mult(RCdata.ch2,num0p5);
                            _Q16 cosRoll = _Q16cos(halfRoll);
                            _Q16 sinRoll = _Q16sin(halfRoll);
                            _Q16 cosPitch = _Q16cos(halfPitch);
                            _Q16 sinPitch = _Q16sin(halfPitch);
                            CmdData.q_cmd.o = mult(cosRoll,cosPitch);
                            CmdData.q_cmd.x = mult(sinRoll,cosPitch);
                            CmdData.q_cmd.y = mult(cosRoll,sinPitch);
                            CmdData.q_cmd.z = -mult(sinRoll,sinPitch);
                            CmdData.p = CmdData.q = 0;
                            CmdData.r = mult(RCdata.ch3,num2p0);
#ifdef BQXX
                            if (CmdData.throttle < 42)
                                CmdData.throttle = 0;
                            else
                                CmdData.throttle -= 42;
#endif
                        } else
                        {
                            CmdData.AttCmd = 0;
                        }

                        // If we are getting RC data, we should correct the gyros to zero
                        loop.ViconCorrect = 1;
                    }
                    rxState_uart1 = RXSTATE_STXA;

                    break;

                default:
                    break;

            }
        }
    #endif
}

void UART1_FlushTX(void){
    // Transfer in progress?
    if(UART1txInProgress)
        return;

    // Are there bytes to send?
    if(UART1txPtr == 0)
        return;

    DMA3CNT = UART1txPtr - 1; // Set the number of transfers
    if(UART1txBuffSel == 0){
        DMA3STA = __builtin_dmaoffset(UART1txBuffA);
        UART1txBuffSel = 1;
    }else{
        DMA3STA = __builtin_dmaoffset(UART1txBuffB);
        UART1txBuffSel = 0;
    }

    UART1txPtr = 0;
    UART1txInProgress = 1;
    DMA3CONbits.CHEN = 1;// Enable DMA3 Channel
    DMA3REQbits.FORCE = 1;// Manual mode: Kick-start the 1st transfer
}

// DMA TX interrupt handler
void __attribute__((__interrupt__)) _DMA3Interrupt(void)
{
    UART1txInProgress = 0;
    IFS2bits.DMA3IF = 0;// Clear the DMA3 Interrupt Flag;
}
