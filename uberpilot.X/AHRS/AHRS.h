
#ifndef _AHRS_H
#define _AHRS_H

#include "../defines.h"

// Prototypes
void AHRS_init(void);
void AHRS_GyroProp(void);
void AHRS_ViconCorrect(void);
void AHRS_AccMagCorrect(void);
void Controller_Update(void);
void Controller_Init(void);

void BMP180_calc_tmperature(void);
void BMP180_calc_pressure(void);

// Structs
typedef struct __attribute__ ((packed)) sSensorData{

    int16_t gyroX;
    int16_t gyroY;
    int16_t gyroZ;

    int16_t accX;
    int16_t accY;
    int16_t accZ;

    int16_t magX;
    int16_t magY;
    int16_t magZ;

    int16_t sonarZ;

    int32_t pressure;
    int32_t temperature;

    //filter buffers for pressure and temperature -- should be multiples of four
    int32_t temperature_buff[4];
    uint8_t temperature_index;
    int32_t temperature_filt;
    int32_t temperature_buff_sum;
    int32_t pressure_buff[128];
    uint8_t pressure_index;
    int32_t pressure_filt;
    int32_t pressure_buff_sum;


    int8_t current1;
    int8_t maxPWM1;
    int8_t temp1;
    
    int8_t current2;
    int8_t maxPWM2;
    int8_t temp2;

    int8_t current3;
    int8_t maxPWM3;
    int8_t temp3;

    int8_t current4;
    int8_t maxPWM4;
    int8_t temp4;

}tSensorData;

typedef struct __attribute__ ((packed)) strQuaternion{
    _Q16 o;
    _Q16 x;
    _Q16 y;
    _Q16 z;
}tQuaternion;

tQuaternion qprod(tQuaternion a, tQuaternion b);
tQuaternion qprodconj(tQuaternion a, tQuaternion b);

typedef struct __attribute__ ((packed)) sAHRSdata{
    _Q16 p;
    _Q16 q;
    _Q16 r;

    _Q16 ax;
    _Q16 ay;
    _Q16 az;

    tQuaternion q_est;
    tQuaternion q_meas;
}tAHRSdata;

typedef struct __attribute__ ((packed)) sSensorCal{

    _Q16 pBias;
    _Q16 qBias;
    _Q16 rBias;
    _Q16 gyroScale;
    _Q16 gyroPropDT;

    _Q16 accelScale;
    _Q16 acc_window_min;
    _Q16 acc_window_max;

    _Q16 K_AttFilter;
    _Q16 kGyroBias;

    int16_t biasCount;
    int16_t biasTotal;
    int16_t blankReads;

    // see data sheet for BMP180;
    int16_t AC1;
    int16_t AC2;
    int16_t AC3;
    uint16_t AC4;
    uint16_t AC5;
    uint16_t AC6;
    int16_t B1;
    int16_t B2;
    int16_t MB;
    int16_t MC;
    int16_t MD; // end calibration data

    int32_t B3;
    uint32_t B4;
    int32_t B5;
    int32_t B6;
    uint32_t B7;
    int32_t X1;
    int32_t X2;
    int32_t X3;
    int32_t UT;
    int32_t UP;
    uint8_t oss;

    uint8_t pressureConversionTime;
    uint8_t temperatureConversionTime;
    uint8_t conversionTime;

}tSensorCal;

typedef struct __attribute__ ((packed)) sCmdData{
    _Q16 p;
    _Q16 q;
    _Q16 r;

    tQuaternion q_cmd;

    uint8_t throttle;
    int8_t collective;
    uint8_t AttCmd;

}tCmdData;

typedef struct __attribute__ ((packed)) sMotorData{

    // motors
    uint8_t m1;
    uint8_t m2;
    uint8_t m3;
    uint8_t m4;

    // servos
    uint8_t s1;
    uint8_t s2;
    uint8_t s3;
    uint8_t s4;

}tMotorData;

//----------------------------------------------------------------------		

typedef struct __attribute__ ((packed)) sGains
{
    _Q16 Kp_roll;
    _Q16 Ki_roll;
    _Q16 Kd_roll;

    _Q16 Kp_pitch;
    _Q16 Ki_pitch;
    _Q16 Kd_pitch;

    _Q16 Kp_yaw;
    _Q16 Ki_yaw;
    _Q16 Kd_yaw;

    _Q16 PWM1_trim;
    _Q16 PWM2_trim;
    _Q16 PWM3_trim;
    _Q16 PWM4_trim;

    _Q16 IntRoll;
    _Q16 IntPitch;
    _Q16 IntYaw;
    _Q16 dt;

    _Q16 maxang;
    _Q16 motorFF;
    uint16_t lowBatt;

    uint8_t stream_data;

}tGains;

typedef struct __attribute__ ((packed)) sRCdata{

    // motors
    uint16_t ch0;
    _Q16 ch1;
    _Q16 ch2;
    _Q16 ch3;
    uint16_t ch4;
    uint16_t ch5;
    uint16_t ch6;

}tRCdata;

#endif